Name:           pass-dmenu-wl
Version:        1
Release:        1%{?dist}
Summary:        Adds dmenu-wl sumbolic link to bemenu
BuildArch:      noarch

License:        GPL

Requires:       bemenu
Supplements:    pass

%description
Dummy package creating symbolic link to `bemenu'.
Required for `passmenu' to work on Wayland

%prep

%build

%install
install -d %{buildroot}%{_bindir}
ln -s bemenu %{buildroot}%{_bindir}/dmenu-wl

%check


%files
%{_bindir}/dmenu-wl

%changelog
* Tue Dec 14 2021 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1-1
- Initial build
