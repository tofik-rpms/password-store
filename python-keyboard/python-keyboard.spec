# Created by pyp2rpm-3.3.7
%global pypi_name keyboard
%global pypi_version 0.13.5

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        2%{?dist}
Summary:        Hook and simulate keyboard events on Windows and Linux

License:        MIT
URL:            https://github.com/boppreh/keyboard
Source0:        %{pypi_source %{pypi_name} %{version} zip}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%description
Take full control of your keyboard with this small Python library. Hook global
events, register hotkeys, simulate key presses and much more. Features-
**Global event hook** on all keyboards (captures keys regardless of focus). -
**Listen** and **send** keyboard events. - Works with **Windows** and **Linux**
(requires sudo), with experimental **OS X** support (thanks @glitchassassin!).
- **Pure...

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name}
Take full control of your keyboard with this small Python library. Hook global
events, register hotkeys, simulate key presses and much more. Features-
**Global event hook** on all keyboards (captures keys regardless of focus). -
**Listen** and **send** keyboard events. - Works with **Windows** and **Linux**
(requires sudo), with experimental **OS X** support (thanks @glitchassassin!).
- **Pure...


%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE.txt
%doc README.md
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%changelog
* Tue Jan 04 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.13.5-2
- Removed unneeded dependency

* Tue Jan 04 2022 mockbuilder - 0.13.5-1
- Initial package.
