# Created by pyp2rpm-3.3.7
%global pypi_name upass
%global pypi_version 0.3.0

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        3%{?dist}
Summary:        Console UI for pass

License:        BSD-3-Clause
URL:            https://github.com/Kwpolska/upass
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  python3dist(sphinx)

%description
%{summary}

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3-libs
Requires:       python3dist(pyperclip)
Requires:       python3dist(setuptools)
Requires:       python3dist(urwid)

# Allow install upass
Provides:       upass = %{version}-%{release}

%description -n python3-%{pypi_name}
The upass. Console UI for pass. 

%package -n python-%{pypi_name}-doc
Summary:        upass documentation
%description -n python-%{pypi_name}-doc
Documentation for upass

%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build
# generate html docs
PYTHONPATH=${PWD} sphinx-build-3 docs html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE docs/LICENSE.rst
%doc README.rst docs/README.rst
%{_bindir}/upass
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%files -n python-%{pypi_name}-doc
%doc html
%license LICENSE docs/LICENSE.rst

%changelog
* Thu Apr 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.3.0-3
- Fixed linter complains

* Mon Dec 06 2021 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 0.3.0-2
- Fixed dependencies
- Added alias

* Thu Dec 02 2021 mockbuilder - 0.3.0-1
- Initial package.
