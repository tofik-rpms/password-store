Name:           tessen
Version:        2.2.3
Release:        1%{?dist}
Summary:        A bash script that can use for auto-typing and copying password store data
BuildArch:      noarch

License:        GPLv2
URL:            https://github.com/ayushnix/tessen
Source0:        https://github.com/ayushnix/tessen/releases/download/v%{version}/tessen-%{version}.tar.gz

BuildRequires:  scdoc
BuildRequires:  make
Requires:       bash
Requires:       pass
Requires:       bemenu
Recommends:     wtype
Recommends:     wl-clipboard
Recommends:     pass-otp
Recommends:     libnotify


%description
A bash script that can use bemenu, rofi, or wofi as an interface
for auto-typing and copying password store data.
tessen is written to work only on Wayland.
If you'd rather use fzf to copy your password-store data on both
Xorg/X11 and Wayland, check out pass-tessen.


%prep
%autosetup


%build


%install
%make_install PREFIX=%{_prefix}


%check


%files
%license LICENSE
%doc README.md
%config(noreplace) %{_sysconfdir}/xdg/tessen
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_datadir}/bash-completion/completions/*
%{_datadir}/fish/vendor_completions.d/*


%changelog
* Sun Mar 10 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.2.3-1
- Update to 2.2.3

* Thu Apr 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.2.1-1
- Update to 2.2.1

* Mon Oct 03 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.1.2-1
- Update to 2.1.2

* Wed Mar 23 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.1.1-1
- Update to 2.1.1

* Thu Feb 22 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.0-1
- Update to 2.0.0

* Mon Jan 24 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.3.1-1
- Update to 1.3.1

* Mon Jan 17 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.3.0-1
- Update to 1.3.0

* Thu Jan 06 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.3-2
- Fix for backend configuration added

* Wed Jan 05 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.3-1
- Initial package
