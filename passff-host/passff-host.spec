%define debug_package %{nil}

Name:           passff-host
Version:        1.2.4
Release:        1%{?dist}
Summary:        Host app for the WebExtension PassFF

License:        GPL
URL:            https://github.com/passff/passff-host
Source0:        https://github.com/passff/passff-host/archive/refs/tags/%{version}/passff-host-%{version}.tar.gz
Patch0:         passff-host-destdir-support.patch

BuildRequires:  make
BuildRequires:  pass
BuildRequires:  python3 >= 3.5
Requires:       pass
Requires:       python3 >= 3.5

%description
%{summary}

%package firefox
Summary: %{summary} contains Firefox binaries
Requires: %{name} == %{version}
Requires: firefox
Enhances: firefox

%description firefox
%{summary}

%package librewolf
Summary: %{summary} contains Librewolf binaries
Requires: %{name} == %{version}
Requires: librewolf
Enhances: librewolf

%description librewolf
%{summary}
Librewolf package can be obtained from Fedora COPR `bgstack15/AfterMozilla`

%package chromium
Summary: %{summary} contains Chromium binaries
Requires: %{name} == %{version}
Enhances: chromium
Enhances: chromium-freeworld

%description chromium
%{summary}

%package chrome
Summary: %{summary} contains Chrome binaries
Requires: %{name} == %{version}
Requires: google-chrome
Enhances: google-chrome

%description chrome
%{summary}

%prep
%autosetup -p1


%build

%install

for browser in firefox librewolf chrome chromium;do
%make_install BROWSER=${browser} VERSION=%{version} LIBDIR=%{_libdir}
done

%check

%files
%license LICENSE
%doc README.md

%files firefox
%defattr(-,root,root,-)
%{_libdir}/mozilla/native-messaging-hosts/*

%files librewolf
%defattr(-,root,root,-)
%{_prefix}/lib/librewolf/native-messaging-hosts/*

%files chromium
%defattr(-,root,root,-)
%{_sysconfdir}/chromium/native-messaging-hosts/*

%files chrome
%defattr(-,root,root,-)
%{_sysconfdir}/opt/chrome/native-messaging-hosts/*

%changelog
* Mon Nov 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.4-1
- Update to 1.2.4

* Mon Dec 27 2021 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.3-4
- Updated Native messaging directory for Librewolf official RPM

* Fri Dec 17 2021 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.3-3
- Added browsers as dependencies to subpackages

* Thu Dec 16 2021 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.3-2
- Changed build architecture

* Thu Dec 16 2021 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.3-1
- Initial build

