# Created by pyp2rpm-3.3.7
%global pypi_name pass-import
%global pypi_version 3.5

Name:           pass-import
Version:        %{pypi_version}
Release:        1%{?dist}
Summary:        A pass extension for importing data from most of the existing password manager

License:        GPL3
URL:            https://github.com/roddhjav/pass-import
Source0:        https://github.com/roddhjav/pass-import/releases/download/v%{version}/%{name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  python3dist(zxcvbn)
BuildRequires:  python3dist(pypandoc)
BuildRequires:  python3dist(dominate)
BuildRequires:  python3dist(requests)
BuildRequires:  python3dist(pyaml)

Requires:       python3dist(requests)
Requires:       python3dist(cryptography)
Requires:       python3dist(defusedxml)
Requires:       python3dist(file-magic)
Requires:       python3dist(pyaml)
Requires:       python3dist(pykeepass)
Requires:       python3dist(secretstorage)
Requires:       python3dist(setuptools)
Requires:       python3dist(zxcvbn)
Requires:       pass

Enhances:       pass

Provides:       python3-%{pypi_name} = %{version}

%description
pass-import extends pass utility with import capability


%prep
%autosetup
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files
%license LICENSE
%doc README.md
%{_bindir}/pimport
%{python3_sitelib}/pass_import
%{python3_sitelib}/pass_import-%{pypi_version}-py%{python3_version}.egg-info
%{_mandir}/man1/*
%{_prefix}/lib/password-store/extensions/*
%{_datadir}/bash-completion/completions/*
%{_datadir}/zsh/site-functions/*

%changelog
* Mon Nov 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 3.4-1
- Update to version 3.5

* Thu Apr 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 3.4-1
- Update to version 3.4

* Mon Oct 03 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 3.3-1
- Update to version 3.3

* Fri Nov 26 2021 mockbuilder - 3.2-3
- Fixed dependencies

* Fri Nov 26 2021 mockbuilder - 3.2-2
- Added weak dependency to pass

* Fri Nov 26 2021 mockbuilder - 3.2-1
- Initial package.
