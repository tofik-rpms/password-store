# Created by pyp2rpm-3.3.7
%global pypi_name pyaml
%global pypi_version 24.9.0

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        2%{?dist}
Summary:        PyYAML-based module to produce pretty and readable YAML-serialized data

License:        WTFPL
URL:            https://github.com/mk-fg/pretty-yaml
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(pyyaml)
BuildRequires:  python3dist(setuptools)
BuildRequires:  python3dist(unidecode)

%description
pretty-yaml (or pyaml) PyYAML-based python module to produce pretty and
readable YAML-serialized data.This module is for serialization only, see
ruamel.yaml_ module for literate YAML parsing (keeping track of comments,
spacing, line/column numbers of values, etc).[note: to dump stuff parsed by
ruamel.yaml with this module, use only YAML(typ'safe') there].. contents::
:backlinks: none- Prime...

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(pyyaml)
%description -n python3-%{pypi_name}
pretty-yaml (or pyaml) PyYAML-based python module to produce pretty and
readable YAML-serialized data.This module is for serialization only, see
ruamel.yaml_ module for literate YAML parsing (keeping track of comments,
spacing, line/column numbers of values, etc).[note: to dump stuff parsed by
ruamel.yaml with this module, use only YAML(typ'safe') there].. contents::
:backlinks: none- Prime...


%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%check
#%{__python3} setup.py test

%files -n python3-%{pypi_name}
%doc README.rst
%{_bindir}/*
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%changelog
* Mon Nov 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 24.9.0-2
- Disabled tests

* Mon Nov 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 24.9.0-1
- Update to 24.9.0

* Tue Dec 12 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 23.9.7-1
- Update to 23.9.7

* Fri Nov 26 2021 mockbuilder - 21.10.1-1
- Initial package.
