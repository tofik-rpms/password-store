Name:           tomb
Version:        2.11
Release:        1%{?dist}
Summary:        A minimalistic commandline tool to manage encrypted volumes aka The Crypto Undertaker
BuildArch:      noarch

License:        GPLv3
URL:            https://www.dyne.org/software/tomb/
Source0:        https://files.dyne.org/tomb/Tomb-%{version}.tar.gz

BuildRequires:  make
BuildRequires:  zsh
BuildRequires:  cryptsetup
BuildRequires:  gettext
BuildRequires:  sudo
BuildRequires:  gnupg2
BuildRequires:  util-linux
Requires:       zsh
Requires:       cryptsetup
Requires:       gnupg2
Requires:       pinentry
Requires:       sudo

%description
Tomb aims to be a free and open source system for easy encryption and backup of personal files, written in code that is easy to review and links well reliable GNU/Linux components.

Tomb's ambition is to improve safety by way of:

    a minimalist design consisting in small and well readable code
    facilitation of good practices, i.e: key/storage physical separation
    adoption of a few standard and well tested implementations.

At present, Tomb consists of a simple shell script (Zsh) using standard filesystem tools (GNU) and the cryptographic API of the Linux kernel (cryptsetup and LUKS). Tomb can also produce machine parsable output to facilitate its use inside graphical applications.


%prep
%autosetup -n Tomb-%{version}


%build


%install
%make_install PREFIX=%{_prefix}
%make_install PREFIX=%{_prefix} -C extras/translations


%check


%files
%doc *.txt
%{_bindir}/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man?/%{name}.?.*


%changelog
* Mon Nov 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.11-1
- Update to 2.11

* Tue Jan 04 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.9-1
- Initial package
