Version:        1.3
%global forgeurl https://github.com/roddhjav/pass-tomb
%forgemeta

Name:           pass-tomb
Release:        2%{?dist}
Summary:        A pass extension to keep the whole tree of password encrypted inside a Tomb

BuildArch:      noarch

License:        GPLv3
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  ShellCheck
BuildRequires:  make
BuildRequires:  pass
BuildRequires:  tomb
BuildRequires:  systemd-rpm-macros
BuildRequires:  kcov
BuildRequires:  e2fsprogs
Requires:       pass >= 1.7.0
Requires:       tomb >= 2.6
Requires:       e2fsprogs

%description
Due to the structure of pass, file- and directory names are not encrypted
in the password store. pass-tomb provides a convenient solution
to put your password store in a Tomb and then keep your password
tree encrypted when you are not using it.

It uses the same GPG key to encrypt passwords and tomb,
therefore you don't need to manage more key or secret. Moreover,
you can ask pass-tomb to automatically close your store after a given time.

%prep
%forgeautosetup


%build


%install
%make_install


%check
%{__make} lint


%files
%license LICENSE
%doc README.md
%{_mandir}/man1/*
%{_prefix}/lib/password-store/extensions/*
%{_datadir}/bash-completion/completions/*
%{_datadir}/zsh/site-functions/*
%{_unitdir}/*

%changelog
* Thu Apr 13 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.3-2
- Removed user timer patch

* Tue Jan 04 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.3-1
- Initial package

