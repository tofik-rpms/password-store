%global debug_package %{nil}
%global forgeurl https://github.com/palortoff/pass-extension-tail
Name:           pass-extension-tail
Version:        1.2.0
%forgemeta
Release:        1%{?dist}
BuildArch:      noarch
Summary:        An extension for the password store
Provides:       pass-tail

License:        GPLv3 
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  make
BuildRequires:  pass
BUildRequires:  dos2unix
Requires:       pass > 1.7.0

%description
An extension for the password store that allows to display and edit
password meta data without displaying the password itself to bystanders

%prep
%forgeautosetup

%build
dos2unix README.md

%install
%make_install

%check


%files
%license
%doc README.md
%{_mandir}/man1/*
%{_prefix}/lib/password-store/extensions/*
%{_sysconfdir}/bash_completion.d/pass-tail

%changelog
* Sun Mar 10 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 1.2.0-1
- Initial build

