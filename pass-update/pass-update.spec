Name:           pass-update
Version:        2.2.1
Release:        1%{?dist}
Summary:        A pass extension that provides an easy flow for updating passwords.
BuildArch:      noarch

License:        GPL
URL:            https://github.com/roddhjav/pass-update
Source0:        https://github.com/roddhjav/pass-update/releases/download/v%{version}/pass-update-%{version}.tar.gz

BuildRequires:  make
BuildRequires:  pass
Requires:       pass >= 1.7.0

%description
pass update extends the pass utility with an update command providing an easy flow for updating passwords.
It supports path, directory and wildcard update.
Moreover, you can select how to update your passwords by automatically generating
new passwords or manually setting your own.
pass update assumes that the first line of the password file is the password
and so only ever updates the first line unless the --multiline option is specified.
By default, pass update prints the old password
and waits for the user before generating a new one.
This behaviour can be changed using the provided options.

%prep
%autosetup


%build


%install
%make_install

%check
%{__make} tests

%files
%license LICENSE
%doc README.md
%{_mandir}/man1/*
%{_prefix}/lib/password-store/extensions/*
%{_datadir}/bash-completion/completions/*
%{_datadir}/zsh/site-functions/*


%changelog
* Mon Nov 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.2.1-1
- Update to 2.2.1
Thu Apr 13 2023
* Mon Dec 13 2021 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 2.1-1
- Initial package.

